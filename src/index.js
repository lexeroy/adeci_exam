// fontawesome imports
import 'normalize.css/normalize.css'
import '@fortawesome/fontawesome-free/js/fontawesome'
import '@fortawesome/fontawesome-free/js/solid'
import '@fortawesome/fontawesome-free/js/regular'
import '@fortawesome/fontawesome-free/js/brands'

import $ from 'jquery'
import 'bootstrap'

// js map
import './scripts/jsmaps-libs'
import './styles/jsmaps.css'
import './scripts/jsmaps'
import './scripts/philippines'

// styles import
import './styles/index.scss'
import './styles/nav.scss'
import './styles/content.scss'
import './styles/rangeslider.scss'

const getElementWithClass = (className) => { return document.getElementsByClassName(className)[0] }
const home = { // home component
    sidenav : getElementWithClass('filter'),
    navbarToggle : getElementWithClass('navbar-toggler'),
    sidenavOnToggle : getElementWithClass('open-sidenav'),
    sidenavOffToggle : getElementWithClass('filter-toggle'),
    isAboveTablet : window.matchMedia("screen and (min-width:768px)").matches,
    map : getElementWithClass('map'),
    provinceDetail : getElementWithClass('province-details'),
    currentProvinceName : getElementWithClass('province-name'),
    closeButton : getElementWithClass('close')
}

document.addEventListener("DOMContentLoaded", () => {
    const toggleSideNav = (tval) => {
        home.sidenav.style.marginLeft = tval ? "0px" : "-250px"
        home.sidenav.style.transform = tval ? "none" : "rotateY(100deg)" 
        home.sidenavOnToggle.style.display = tval ? "none" : "block"
        home.map.style.marginLeft = "0px"
    }

    home.navbarToggle.onclick = () => {
        let containsActive =  home.navbarToggle.classList.contains('is-active')
        home.navbarToggle.classList[containsActive ? 'remove' : 'add']('is-active')
    }

    home.sidenavOffToggle.onclick = () => toggleSideNav(false)
    home.sidenavOnToggle.onclick = () => toggleSideNav(true)

    home.closeButton.onclick =() => {
        home.map.style.marginLeft = "0"
        home.provinceDetail.style.display = 'none'
    }

    window.onresize = () => {
        home.isAboveTablet = window.matchMedia("screen and (min-width:768px)").matches
        toggleSideNav(home.isAboveTablet)
    }

    // page init
    $('#phmap').JSMaps({  
        map:'philippines',
        selectElement: false,
        displayAbbreviations: true,
        displayViewBox: true,
        responsive: true,
        onStateClick: (data) => {
            let isProvincePageOpen = home.provinceDetail.style.display == 'block'
            if (data.name !== home.currentProvinceName.dataset.info) {
                home.currentProvinceName.dataset.info = data.name
                home.currentProvinceName.innerHTML = `Province of ${data.name}`
                home.provinceDetail.style.display = 'block'
                home.map.style.marginLeft = home.isAboveTablet ? "0" : "-120%"
                home.currentProvinceName.closest('.header').style.backgroundColor  = data.color
                getElementWithClass('province-info').innerHTML = data.text
            } else {
                home.provinceDetail.style.display = isProvincePageOpen ? 'none' : 'block'
            }
        }
    })
    
    setTimeout(() => $(".preloader").fadeOut("slow"), 1000) 
    toggleSideNav(home.isAboveTablet)
});
